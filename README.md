# osx and qlab configuration

Ansible role to configure defaults on OSX. Based on 
[lafarer/ansible-role-osx-defaults](https://github.com/lafarer/ansible-role-osx-defaults) but seriously stripped down and
reworked for our purpose to manage Mac OSX machines running
QLab.

Many of the parts of this role have been removed, to reinstall 
them, please refer to the original role.

## Requirements

Ansible 2.0

## Role Variables

```
AppStore_Enabled: false                          # Enable AppStore configuration
AppStore_AutomaticCheckEnabled: false            # Automatically check for updates
AppStore_AutomaticDownload: false                # Download newly available updates in the background
AppStore_AutoUpdate: false                       # Install app updates
AppStore_AutoUpdateRestartRequired: false        # Install OSX updates
AppStore_ConfigDataInstall: false                # Install system data files
AppStore_CriticalUpdateInstall: false            # Install security updates
AppStore_ScheduleFrequency: 7                    # Check for software updates (in days)
AppStore_ShowDebugMenu: false                    # Show debug menu
```

```
Qlab_Enabled: true                               # Enable Qlab configuration
Qlab_AutomaticUpdateEnabled: false               # Disable automatic updates
Qlab_SpotlightDisabled: true                     # Disable spotlight
Qlab_DisplaySleep: 0                             # Disable Display sleep
Qlab_DiskSleep: 0                                # Disable Disk sleep
Qlab_Sleep: 0                                    # Disable System sleep
Qlab_ScreensaverTimeout: 0                       # Disable Screensaver
Qlab_DashboardDisabled: true                     # Disable dashboard
Qlab_TimeMachineDisabled: true                   # Disable TimeMachine
Qlab_IcloudDisabled: true                        # Disable iCloud
```

## Dependencies

- geerlingguy.homebrew

Homebrew role for installing software on OSX machines.

- qlab

OS X devices at Naturalis run qlab. The qlab settings are mostly
based on the suggestions on this page which describes how to
[prepare your mac when using qlab](https://figure53.com/docs/qlab/v4/general/preparing-your-mac/).


## Example Playbook

```
    - hosts: servers
      vars:
        AppStore_Enabled: true
        AppStore_AutomaticCheckEnabled: false

      roles:
         - { role: naturalis.osx }
```

## License

BSD
